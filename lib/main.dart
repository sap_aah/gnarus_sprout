import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Gnarus Monitor Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MonitorPage(title: 'Monitor'),
    );
  }
}

class MonitorPage extends StatelessWidget {
  MonitorPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(title: Text(this.title)),
        body: Container(
          // Adding padding here to make the screen less squashed
          padding: EdgeInsets.all(16),
          // We are getting an overflow error, because our content is not scrolling
          // I will fix this with a SingleChildScrollView
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Text(
                  'Tunnel Site 1',
                  textAlign: TextAlign.left,
                  style: Theme.of(context).textTheme.title,
                ),
                // Adding sized boxes here to add space between the elements in the Column
                SizedBox(height: 16),
                ChartTile(title: 'Front Sensor 1'),
                SizedBox(height: 16),
                ChartTile(title: 'Back Sensor 1'),
              ],
            ),
          ),
        ),
      );
}

class ChartTile extends StatelessWidget {
  final String title;

  ChartTile({this.title});

  BoxDecoration get containerDecoration =>
      BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(color: Colors.black12, blurRadius: 6.0, offset: Offset(0, 3))
      ]);

  @override
  Widget build(BuildContext context) {
    return Container(
      // I want to add some padding to this container, so it doesn't look crowded
      padding: EdgeInsets.all(16),
      // I want to add a decoration to this container so that it has a drop shadow
      decoration: this.containerDecoration,
      child: Column(
        children: <Widget>[
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Text(
                  this.title,
                  textAlign: TextAlign.left,
                  style: Theme.of(context).textTheme.subtitle,
                ),
                SizedBox(height: 8),
                Row(
                  children: <Widget>[
                    Chip(label: Text('27 Mar 2.02pm')),
                    SizedBox(
                      width: 16,
                    ),
                    Chip(label: Text('63.70 °C')),
                    SizedBox(
                      width: 16,
                    ),
                    Chip(label: Text('35.63 °C'))
                  ],
                )
              ],
            ),
          ),
          // I will put a placeholder in for the chart for now
          // We need to download an external library to use it, it is not included in flutter
          // https://pub.dev/packages/charts_flutter
          Container(
            child: Text('Chart goes here'),
            height: 250,
          ),
          Container(
            alignment: Alignment.topLeft,
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(
                      color: Colors.blue, style: BorderStyle.solid, width: 1)),
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: DropdownButton(
                  // I am leaving this method empty, as choosing an item from the list isn't supported yet by our code
                  onChanged: (_) {},
                  items: [
                    // Here are all the available menu items
                    DropdownMenuItem(child: Text('ONE HOUR')),
                    DropdownMenuItem(child: Text('ONE DAY')),
                    DropdownMenuItem(child: Text('ONE WEEK')),
                    DropdownMenuItem(child: Text('ALL')),
                  ]),
            ),
          )
        ],
      ),
    );
  }
}

class ChartWidget extends StatelessWidget {
  List<charts.Series> seriesList;

  @override
  Widget build(BuildContext context) => new charts.LineChart(seriesList);
}
